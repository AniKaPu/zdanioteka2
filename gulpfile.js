var gulp = require('gulp');

gulp.task('default', function() {
});

/*var gulp = require('gulp');

gulp.task('default', function() {
 });
    var concat = require('gulp-concat');
 
gulp.task('scripts', function() {
  return gulp.src(['first.js', 'second.js'])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./'));
});*/

var sass = require('gulp-sass');
 
gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css')); //uważać, zeby plik html był w tym samym folderze
});
 
gulp.task('sass_watch', function () {
gulp.watch('./sass/**/*.scss',
          gulp.parallel('sass'));// to jest v 4.0  ta linijka będzie wyglądac nieco inaczej dla wersji 4.1.0, to jest wersja 3.1 sprawdzić w cdn komenda gulp -v
});

/*const autoprefixer = require('gulp-autoprefixer'); // zistlowć to jpierw w cosoli poleceie ze stroy potem to wpisc:
 
gulp.task('default', () =>
    gulp.src('src/app.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
);*/

const babel = require('gulp-babel');

gulp.task('babel', () =>
   gulp.src('./zdanioteka.js')
       .pipe(babel({
           presets: ['@babel/env']
       }))
       .pipe(gulp.dest('dist'))
);